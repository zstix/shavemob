$(function() {

	// ----- PRODUCT DETAILS ---- \\

	// open up the product details
	$('ul.products li .more-details').click(function(e) {

		var step = $(e.target).parents('li');
		step.find('.product-info').fadeIn();

	});

	// close the product details
	$('ul.products li .product-info .close').click(function(e) {

		var info = $(e.target).parents('.product-info');
		info.fadeOut();

	});

	// ----- PRODUCT SELECTION STEPS ----- \\

	// when they click on the select button
	$('ul.products li .select_btn').click(function(e) {

		// hide all of the other steps and show all the buttons
		$('ul.products li .step-one').hide();
		$('ul.products li .step-two').hide();
		$('ul.products li .select_btn').show();

		// save the product li for easier use
		var product = $(e.target).parents('li');

		// hide the select button
		$(e.target).hide();

		// look for step one
		var step_one = product.find('.step-one');

		// if there is a step one (third product)
		if (step_one.length != 0) {

			// show the step and scroll to it
			step_one.fadeIn();
			$(window).scrollTop(step_one.offset().top);

		// otherwise, there is no step one (first two products)
		} else {

			// just show the second step with no extra options
			step.find('.step-two').fadeIn();

		}

	});

	// when they click on a handle option in step one
	$('ul.products li .step-one .select').click(function(e) {

		// save the product li for easier use
		var product = $(e.target).parents('li');

		// figure out what option they chose and which they didnt
		var optionDiv = $(e.target).parents('.step-one-option');
		var option = optionDiv.hasClass('a') ? 'a' : 'b';
		var antiOption = (option == 'a') ? 'b' : 'a';

		// hide the first step
		product.find('.step-one').hide();

		// show the second step with the right option (and the other one hidden in case)
		product.find('.step-two .' + option).show();
		product.find('.step-two .' + antiOption).hide();
		product.find('.step-two').fadeIn();

	});

});